package main;

import entities.Track;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainExample {


    public static void main(String[] args) {

        Map<String, String> map = new HashMap<>();

        System.err.println(map.put("1", "A"));
        System.err.println(map.put("1", "B"));
        System.err.println(map.put("1", "c"));
        System.err.println("_____________________________________");

        //Преобразование строк в верхний регистр с помощью map
        List<String> collected = Stream.of("a", "b", "hello")
                .map(string -> string.toUpperCase())
                .collect(Collectors.toList());
        collected.forEach(System.out::println);
        System.out.println("____________________________________________________________________________________");

        //Отфильтровать строки начинающиеся с цифры 4.
        List<String> filtered = Stream.of("asd3f", "sdf5", "43еак", "2")
                .filter(string -> string.charAt(0) == '4')
                .collect(Collectors.toList());
        filtered.forEach(System.out::println);
        System.out.println("____________________________________________________________________________________");

        //Min and Max длины произведения исполнителя
        List<Track> tracks = Stream.of(new Track("Bakai", 524),
                new Track("Violets for Your Furs", 378),
                new Track("Time Was", 451)).collect(Collectors.toList());
        System.out.println("Shortest tracks: "+
                tracks.stream().min(Comparator.comparing(Track::getLength)).get());
        System.out.println("Longest tracks: " +
                tracks.stream().max(Comparator.comparing(Track::getLength)).get());
        System.out.println("____________________________________________________________________________________");

        //Лямбда-выражение, которое называется редуктором,
        // принимает два аргумента и возвращает их сумму.
        int sum = Stream.of(1, 2, 3)
                .reduce(0, (acc, element) -> acc + element);
        System.out.println(sum);

        String allTrackNames = tracks.stream().map(track -> track.getName()).reduce("Result :", (accumulate, track2) -> accumulate + " / " + track2);
        System.out.println(allTrackNames);
        System.out.println("____________________________________________________________________________________");


        //Метод FlatMap (рис. 3.7) позволяет заменить значение объектом Stream и кон-
        //катенировать все потоки.
        List<Track> together = Stream.of(tracks,tracks,tracks)
                .flatMap(trackStream -> trackStream.stream()).collect(Collectors.toList());
        together.forEach(System.out::println);
    }
}