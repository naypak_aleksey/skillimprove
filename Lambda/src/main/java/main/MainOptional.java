package main;

import java.util.Optional;
import java.util.function.Supplier;

public class MainOptional {

    public static void main(String[] args) {
        //Создание экземпляра Optional по имеющемуся значению
        Optional<String> a = Optional.of("a");
        System.out.println("a".equals(a.get()));

        //Создание пустого экземпляра Optional и проверка наличия значения
        Optional emptyOptional = Optional.ofNullable(null);
        System.out.println(emptyOptional.isPresent());

        //перед каждым вызовом get() проверять наличие значения
        if (a.isPresent()){
            System.out.println(a.get());
        }

        //orElse, кото- рый возвращает альтернативное значение, если экземпляр Optional
        System.out.println(emptyOptional.orElse("Empty"));

        //Если создание альтернативного значения сопряжено с больши- ми накладными расходами,
        // то лучше использовать метод orElseGet.
        System.out.println(emptyOptional.orElseGet(new Supplier() {
            @Override
            public Object get() {
                return "CCC";
            }
        }));
    }
}
