package main;

import entities.Location;
import entities.MusicalGroup;
import entities.Person;
import enums.Cities;
import enums.Countries;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainLambda {
    private static final MusicalGroup BEATELS = new MusicalGroup();
    private static final Set<Person> BEATELS_MEMBERS = new HashSet<>();
    private static final Person JOHN_LENON = new Person("John","Lenon");
    private static final Person PAUL_MCCARTNEY = new Person("Paul","Mccartney");
    private static final Person GEORGE_HARRISON = new Person("George","Harrison");
    private static final Person RINGO_STARR = new Person("Ringo","Starr");
    private static final Location LOCATION = new Location();
    static {
        BEATELS.setPersons(BEATELS_MEMBERS);
        BEATELS.setName("BEATELS");
        BEATELS.setOrigin(LOCATION);
        BEATELS_MEMBERS.add(JOHN_LENON);
        BEATELS_MEMBERS.add(PAUL_MCCARTNEY);
        BEATELS_MEMBERS.add(GEORGE_HARRISON);
        BEATELS_MEMBERS.add(RINGO_STARR);
        LOCATION.setCity(Cities.LIVERPOOL);
        LOCATION.setCountry(Countries.ENGLAND);
    }
    public static void main(String[] args) {

    }
}
