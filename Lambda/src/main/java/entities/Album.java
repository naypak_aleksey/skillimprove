package entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

@Data
@EqualsAndHashCode
@ToString (of = "name")
public class Album {
    private String name;
    private Set<Track> tracks;
    private Set<Person> musicians;
}
