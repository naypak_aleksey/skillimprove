package entities;

import enums.Cities;
import enums.Countries;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString(includeFieldNames = false)
public class Location {
    private Countries country;
    private Cities city;
}
