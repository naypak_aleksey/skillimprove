package entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
@ToString(includeFieldNames = false)
@AllArgsConstructor
@Getter
public class Track {
    private  String name;
    private int length;
}
