package state;

import state.states.ChooseDoctor;
import state.states.Requested;
import state.states.Show;
import state.states.Status;

public class Main {

    static class DoctorColling {
        private Status status;
        private int currentCode = -1;
        private Status requested = new Requested();
        private Status show = new Show();
        private Status chooseDoctor = new ChooseDoctor();


        public void setStatus(Status status) {
            this.status = status;
        }

        public void execute() {
            if (status == null) {
                status = requested;
            }
            status.execute();
            currentCode = status.getCode();
            switch (currentCode) {
                case 1:
                    status = show;
                    break;
                case 2:
                    status = chooseDoctor;
                    break;
                case 3:
                default:
                    status = requested;
            }
        }

    }

    public static void main(String[] args) {
        DoctorColling doctorColling = new DoctorColling();

        for (int i = 0; i <= 20; i++) {
            System.out.println(i+"  _______________________________________________________");
            doctorColling.execute();
        }
    }
}
