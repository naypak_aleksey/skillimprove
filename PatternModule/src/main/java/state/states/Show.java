package state.states;

public class Show implements Status{

    @Override
    public void execute() {
        System.out.println("Show all doctors");
    }

    @Override
    public int getCode() {
        return 2;
    }
}
