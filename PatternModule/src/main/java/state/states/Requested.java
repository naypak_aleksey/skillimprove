package state.states;

public class Requested implements Status{
    @Override
    public void execute() {
        System.out.println("Find doctor");
    }

    @Override
    public int getCode() {
        return 1;
    }
}
