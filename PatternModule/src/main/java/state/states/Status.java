package state.states;

public interface Status {
    void execute();
    int getCode();
}
