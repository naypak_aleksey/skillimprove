package state.states;

public class ChooseDoctor implements Status{
    @Override
    public void execute() {
        System.out.println("Choose doctors");
    }

    @Override
    public int getCode() {
        return 3;
    }
}
