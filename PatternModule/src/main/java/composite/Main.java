package composite;

public class Main {
    public static void main(String[] args) {
        //создание верхнего узла
        MenuComponent allMenus = new Menu("ALL MENUS", "ALL menus combine");

        //создание нижних узлов
        MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE MENU", "Breakfast");
        MenuComponent dinnerMenu = new Menu("DINER MENU", "Launch");
        MenuComponent cafeMenu = new Menu("CAFE MENU", "Dinner");
        MenuComponent dessertMenu = new Menu("DESSERT MENU", "Dessert of course !");

        //добавление нижних узлов или листьев для верхнего
        allMenus.add(pancakeHouseMenu);
        allMenus.add(dinnerMenu);
        allMenus.add(cafeMenu);
        allMenus.add(dessertMenu);

        //добавление листьев
        pancakeHouseMenu.add(new MenuItem(
                "Pancake DISHES ", "Pancake description dishes", false, 11.11
        ));

        dinnerMenu.add(new MenuItem(
                "DINNER MENU DISHES ", "DINNER MENU description dishes", false, 22.22
        ));

        cafeMenu.add(new MenuItem(
                "CAFE MENU DISHES ", "CAFE MENU description dishes", false, 33.33
        ));

        dessertMenu.add(new MenuItem(
                "DESSERT MENU DISHES ", "DESSERT MENU description dishes", false, 44.44
        ));


        //клиентский вызов
        Waitress waitress = new Waitress(allMenus);
        waitress.printMenu();
    }
}
