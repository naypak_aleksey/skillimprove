package decorator;

import decorator.beverages.Beverage;
import decorator.beverages.DarkRoast;
import decorator.beverages.Espresso;
import decorator.beverages.HouseBlend;
import decorator.condiment_decorators.Mocha;
import decorator.condiment_decorators.Soy;
import decorator.condiment_decorators.Whip;

//StarbuzzCoffee Decorator
public class MainDecorator {
    public static void main(String[] args) {
        Beverage espresso = new Espresso();
        System.out.println(espresso.getDescription()+ "$"+espresso.cost());

        Beverage darkRoast = new DarkRoast();
        darkRoast = new Mocha(darkRoast);
        darkRoast = new Mocha(darkRoast);
        darkRoast = new Whip(darkRoast);
        System.out.println(darkRoast.getDescription()+" $"+darkRoast.cost());

        Beverage houseBland = new HouseBlend();
        houseBland = new Mocha(houseBland);
        houseBland = new Whip(houseBland);
        houseBland = new Soy(houseBland);
        System.out.println(houseBland.getDescription()+" $"+houseBland.cost());
    }
}
