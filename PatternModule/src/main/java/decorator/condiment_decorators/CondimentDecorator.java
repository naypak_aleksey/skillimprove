package decorator.condiment_decorators;

import decorator.beverages.Beverage;

public abstract class CondimentDecorator extends Beverage {
    public abstract  String getDescription();
}
