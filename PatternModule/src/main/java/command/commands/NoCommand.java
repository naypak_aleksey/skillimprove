package command.commands;

import command.ICommand;

public class NoCommand implements ICommand {
    @Override
    public void execute() {
        System.out.println("NO COMMAND ......");
    }
}