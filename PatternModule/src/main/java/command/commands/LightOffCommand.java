package command.commands;

import command.ICommand;
import command.devices.LightDevice;

public class LightOffCommand implements ICommand {
    private LightDevice device;

    public LightOffCommand(LightDevice device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.off();
    }
}
