package command.commands;

import command.ICommand;
import command.devices.LightDevice;

public class LightOnCommand implements ICommand {
    private LightDevice lightDevice;

    public LightOnCommand(LightDevice lightDevice) {
        this.lightDevice = lightDevice;
    }

    @Override
    public void execute() {
        lightDevice.on();
    }
}
