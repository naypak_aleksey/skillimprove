package command;

import command.commands.LightOffCommand;
import command.commands.LightOnCommand;
import command.devices.LightDevice;

public class MainCommand {

    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();
        LightDevice lamp = new LightDevice();
        LightOnCommand lightOnCommand = new LightOnCommand(lamp);
        LightOffCommand lightOffCommand = new LightOffCommand(lamp);
        remoteControl.setCommand(0,lightOnCommand,lightOffCommand);

        System.out.println(remoteControl);
        remoteControl.onButtonWasPressed(0);
        remoteControl.offButtonWasPressed(0);
    }

}
