package observer;

import observer.displays.CurrentConditionsDisplay;
import observer.displays.StatisticsDisplay;
import observer.subject.WeatherData;

//Weather station
public class MainObserver {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(weatherData);
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        weatherData.setMeasurements(10,20,30);

    }
}
