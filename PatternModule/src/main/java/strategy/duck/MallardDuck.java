package strategy.duck;

import strategy.duck.behavior.FlyWithWings;
import strategy.duck.behavior.Quack;

public class MallardDuck extends Duck {
    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }
    public void display(){
        System.out.println("I`m a real Mallard duck");
    }
}
