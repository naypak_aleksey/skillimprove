package strategy.duck.behavior;

public class Squeak implements QuackBehavior {
    public void quack() {
        System.out.println("Squeak.... Squeak ....");
    }
}
