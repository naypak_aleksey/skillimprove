package strategy;

import strategy.duck.Duck;
import strategy.duck.MallardDuck;
import strategy.duck.behavior.FlyWithWings;
import strategy.duck.behavior.Quack;

public class MainStrategy {
    public static void main(String[] args) {
        Duck model = new MallardDuck();
        model.setFlyBehavior(new FlyWithWings());
        model.setQuackBehavior(new Quack());
        model.performFly();
        model.performQuack();
    }
}
