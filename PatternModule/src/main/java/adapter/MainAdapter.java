package adapter;

public class MainAdapter {


    public static void main(String[] args) {
        MallardDuck mallardDuck = new MallardDuck();
        WildTurkey wildTurkey = new WildTurkey();
        TurkeyAdapter turkeyAdapter = new TurkeyAdapter(wildTurkey);

        System.out.println("_________MallardDuck____________");
        testDuck(mallardDuck);

        System.out.println("_________TurkeyAdapter____________");
        testDuck(turkeyAdapter);
    }


    private static void testDuck(Duck duck) {
        duck.quack();
        duck.fly();
    }
}
