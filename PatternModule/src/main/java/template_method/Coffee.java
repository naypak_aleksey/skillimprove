package template_method;

public class Coffee extends CaffeineBeverage {

    @Override
    public void brew() {
        System.out.println("Dripping coffee through filter.....");
    }

    @Override
    public void addCondiments() {
        System.out.println("Adding sugar and milk.....");
    }

    public void boilWater() {
        System.out.println("Boiling water....");
    }

    public void pourInCup() {
        System.out.println("Purring into cup.....");
    }

    @Override
    public boolean isCustomerWantsCondiments() {
        return false;
    }
}