                Шаблонный Метод
    Задает скелет алгоритма в методе, оставляя определение некоторых
шагов субклассам.Субкласы могут переопределять некоторые части алгоритма
без изменения его структуры.
    Определяет основные шаги алгоритма и позволяет субклассам
предоставить реализацию одного или нескольких шагов.
