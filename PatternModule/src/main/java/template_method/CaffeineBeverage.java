package template_method;

public abstract class CaffeineBeverage {

    final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        if (isCustomerWantsCondiments()){
            addCondiments();
        }
    }

    public abstract void brew();

    public abstract void addCondiments();

    public void boilWater() {
        System.out.println("Boiling water....");
    }

    public void pourInCup() {
        System.out.println("Purring into cup.....");
    }

    public boolean isCustomerWantsCondiments(){
        return true;
    }
}
