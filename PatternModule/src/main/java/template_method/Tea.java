package template_method;

public class Tea extends CaffeineBeverage {

    @Override
    public void brew() {
        System.out.println("Dripping coffee through filter.....");
    }

    @Override
    public void addCondiments() {
        System.out.println("Adding sugar and lemon.....");
    }

    @Override
    public boolean isCustomerWantsCondiments() {
        return true;
    }
}
