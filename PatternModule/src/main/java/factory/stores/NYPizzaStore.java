package factory.stores;

import factory.pizzas.Pizza;
import factory.pizzas.styles.new_york.NYStyleCheesePizza;
import factory.pizzas.styles.new_york.NYStylePepperoniPizza;
import factory.pizzas.styles.new_york.NYStyleVeggiePizza;

public class NYPizzaStore extends PizzaStore {

    public Pizza createPizza(String type) {
        Pizza pizza;
        switch (type) {
            case VEGGIE:
                pizza = new NYStyleVeggiePizza();
                break;
            case CHEESE:
                pizza = new NYStyleCheesePizza();
                break;
            case PEPPERONI:
                pizza = new NYStylePepperoniPizza();
                break;
            default:
                pizza = null;
        }
        return pizza;
    }
}
