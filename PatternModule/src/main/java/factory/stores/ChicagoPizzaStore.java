package factory.stores;

import factory.pizzas.Pizza;
import factory.pizzas.styles.checkago.ChicagoStyleCheesePizza;
import factory.pizzas.styles.checkago.ChicagoStylePepperoniPizza;
import factory.pizzas.styles.checkago.ChicagoStyleVeggiePizza;

public class ChicagoPizzaStore extends PizzaStore {

    public Pizza createPizza(String type) {
        Pizza pizza;
        switch (type) {
            case VEGGIE:
                pizza = new ChicagoStyleVeggiePizza();
                break;
            case CHEESE:
                pizza = new ChicagoStyleCheesePizza();
                break;
            case PEPPERONI:
                pizza = new ChicagoStylePepperoniPizza();
                break;
            default:
                pizza = null;
        }
        return pizza;
    }
}
