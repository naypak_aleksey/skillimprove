package factory.stores;

import factory.pizzas.Pizza;
import factory.pizzas.styles.california.CaliforniaStyleCheesePizza;
import factory.pizzas.styles.california.CaliforniaStylePepperoniPizza;
import factory.pizzas.styles.california.CaliforniaStyleVeggiePizza;

public class CaliforniaPizzaStore extends PizzaStore {

    public Pizza createPizza(String type) {
        Pizza pizza;
        switch (type) {
            case VEGGIE:
                pizza = new CaliforniaStyleVeggiePizza();
                break;
            case CHEESE:
                pizza = new CaliforniaStyleCheesePizza();
                break;
            case PEPPERONI:
                pizza = new CaliforniaStylePepperoniPizza();
                break;
            default:
                pizza = null;
        }
        return pizza;
    }
}