package factory;

import factory.stores.CaliforniaPizzaStore;
import factory.stores.ChicagoPizzaStore;
import factory.stores.NYPizzaStore;
import factory.stores.PizzaStore;

//Pizza test drive
public class FactoryMain {
    public static void main(String[] args) {
        PizzaStore nyPizzaStore = new NYPizzaStore();
        PizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
        PizzaStore californiaPizzaStore = new CaliforniaPizzaStore();

        nyPizzaStore.orderPizza(PizzaStore.CHEESE);
        System.out.println("--------------------------\n\n\n");
        chicagoPizzaStore.orderPizza(PizzaStore.PEPPERONI);
        System.out.println("--------------------------\n\n\n");
        californiaPizzaStore.orderPizza(PizzaStore.VEGGIE);
        System.out.println("--------------------------\n\n\n");
    }
}
