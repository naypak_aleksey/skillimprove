package factory.pizzas.styles.checkago;

import factory.pizzas.Pizza;

public class ChicagoStyleVeggiePizza implements Pizza {
    public void prepare() {
        System.out.println("PREPARE " + "\"" + "ChicagoStyleVeggiePizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "ChicagoStyleVeggiePizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "ChicagoStyleVeggiePizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "ChicagoStyleVeggiePizza" + "\"");
    }
}
