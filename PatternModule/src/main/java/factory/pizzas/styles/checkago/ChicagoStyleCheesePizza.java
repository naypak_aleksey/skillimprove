package factory.pizzas.styles.checkago;

import factory.pizzas.Pizza;

public class ChicagoStyleCheesePizza implements Pizza {

    public void prepare() {
        System.out.println("prepare " + "\"" + "ChicagoStyleCheesePizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "ChicagoStyleCheesePizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "ChicagoStyleCheesePizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "ChicagoStyleCheesePizza" + "\"");
    }
}
