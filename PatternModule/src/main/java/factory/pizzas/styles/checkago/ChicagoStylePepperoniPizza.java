package factory.pizzas.styles.checkago;

import factory.pizzas.Pizza;

public class ChicagoStylePepperoniPizza implements Pizza {
    public void prepare() {
        System.out.println("PREPARE " + "\"" + "ChicagoStylePepperoniPizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "ChicagoStylePepperoniPizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "ChicagoStylePepperoniPizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "ChicagoStylePepperoniPizza" + "\"");
    }
}
