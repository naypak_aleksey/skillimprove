package factory.pizzas.styles.new_york;

import factory.pizzas.Pizza;

public class NYStyleCheesePizza implements Pizza {

    public void prepare() {
        System.out.println("prepare " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }
}
