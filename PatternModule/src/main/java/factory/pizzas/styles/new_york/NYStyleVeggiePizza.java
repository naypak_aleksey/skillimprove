package factory.pizzas.styles.new_york;

import factory.pizzas.Pizza;

public class NYStyleVeggiePizza implements Pizza {
    public void prepare() {
        System.out.println("PREPARE " + "\"" + "CaliforniaStyleVeggiePizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "CaliforniaStyleVeggiePizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "CaliforniaStyleVeggiePizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "CaliforniaStyleVeggiePizza" + "\"");
    }
}
