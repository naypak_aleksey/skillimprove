package factory.pizzas.styles.new_york;

import factory.pizzas.Pizza;

public class NYStylePepperoniPizza implements Pizza {
    public void prepare() {
        System.out.println("PREPARE " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }
}
