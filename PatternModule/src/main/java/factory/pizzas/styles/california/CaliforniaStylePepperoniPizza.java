package factory.pizzas.styles.california;

import factory.pizzas.Pizza;

public class CaliforniaStylePepperoniPizza implements Pizza {
    public void prepare() {
        System.out.println("PREPARE " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "CaliforniaStylePepperoniPizza" + "\"");
    }
}
