package factory.pizzas.styles.california;

import factory.pizzas.Pizza;

public class CaliforniaStyleCheesePizza implements Pizza {

    public void prepare() {
        System.out.println("prepare " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }

    public void bake() {
        System.out.println("BAKE " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }

    public void cut() {
        System.out.println("CUT " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }

    public void box() {
        System.out.println("BOX " + "\"" + "CaliforniaStyleCheesePizza" + "\"");
    }
}
