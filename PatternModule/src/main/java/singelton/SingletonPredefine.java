package singelton;

public class SingletonPredefine {
    private static final SingletonPredefine INSTANCE;

    static {
        INSTANCE = new SingletonPredefine();
    }

    private SingletonPredefine() {
    }

    public static SingletonPredefine getInstance() {
        return INSTANCE;
    }
}