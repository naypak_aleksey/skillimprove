package singelton;

public class MainSingleton {
    public static void main(String[] args) {
        SingletonLazy singletonLazy = SingletonLazy.getInstance();
        System.err.println("Is initialize singletonLazy"+singletonLazy!=null);
        SingletonPredefine singletonPredefine = SingletonPredefine.getInstance();
        System.err.println("Is initialize singletonLazy" + singletonPredefine != null);
    }
}