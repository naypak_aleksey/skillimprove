                Принципы
1)Инкапсулируйте то что изменяется.
2)Предпочитайте композицию наследованию.
3)Програмируйте на уровне интерфейсов.
4)Стремитесь к слабой связанности взаимодействующих обьектов.
5)Классы должны быть открыты для расширения но закрыты для изменения.
6)Инверсия зависимостей-код должен зависеть от абстракций ,а не от конкретных классов.
7)Принцип минимальной информативности: общайтесь только с близкими друзьями.
8)Голливудский принцип - не вызывайте нас ,мы вызовим вас.
9)Класс должен иметь только 1-у причину для изменения.