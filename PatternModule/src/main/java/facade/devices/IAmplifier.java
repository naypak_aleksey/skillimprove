package facade.devices;

public interface IAmplifier {
    void turnOn();

    void turnOff();
}
