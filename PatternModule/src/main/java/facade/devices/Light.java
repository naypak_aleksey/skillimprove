package facade.devices;

public interface Light {
    void onLight();
    void middleLight();
}
