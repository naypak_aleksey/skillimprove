package facade.devices;

public class Amplifier implements IAmplifier {
    @Override
    public void turnOn() {
        System.out.println("Amplifier turn on.... ");
    }

    @Override
    public void turnOff() {
        System.out.println("Amplifier turn off.... ");
    }
}
