package facade.devices;

public class TV implements ITV {
    @Override
    public void startMovie() {
        System.out.println("TV Start movie....");
    }

    @Override
    public void endMovie() {
        System.out.println("TV End movie....");
    }
}
