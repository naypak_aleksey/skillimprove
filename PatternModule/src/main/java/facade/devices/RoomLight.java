package facade.devices;

public class RoomLight implements Light {
    @Override
    public void onLight() {
        System.out.println("Light switch on....");
    }

    @Override
    public void middleLight() {
        System.out.println("Light set middle...");
    }
}
