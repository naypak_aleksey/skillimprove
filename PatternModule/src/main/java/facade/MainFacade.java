package facade;

import facade.devices.Amplifier;
import facade.devices.IAmplifier;
import facade.devices.ITV;
import facade.devices.Light;
import facade.devices.RoomLight;
import facade.devices.TV;

public class MainFacade {
    public static void main(String[] args) {
        IAmplifier iAmplifier = new Amplifier();
        ITV tv = new TV();
        Light roomLight = new RoomLight();
        HomeTheatreFacade homeTheatreFacade = new HomeTheatreFacade(iAmplifier,tv,roomLight);
        homeTheatreFacade.watchMovie("TERMINATOR");
        homeTheatreFacade.endMovie();
    }
}
