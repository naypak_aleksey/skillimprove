package facade;

import facade.devices.IAmplifier;
import facade.devices.ITV;
import facade.devices.Light;

public class HomeTheatreFacade {
    IAmplifier amplifier;
    ITV tv;
    Light roomLight;

    public HomeTheatreFacade(IAmplifier amplifier, ITV tv, Light roomLight) {
        this.amplifier = amplifier;
        this.tv = tv;
        this.roomLight = roomLight;
    }

    public void watchMovie(String nameMovie){
        System.out.println("_______________"+nameMovie+"_______________");
        amplifier.turnOn();
        roomLight.middleLight();
        tv.startMovie();
    }
    public void endMovie(){
        System.out.println("_______________END_______________");
        amplifier.turnOff();
        roomLight.onLight();
        tv.endMovie();
    }
}